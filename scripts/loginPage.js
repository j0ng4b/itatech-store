function setLoginPage() {
    removeStoreElements();
    addLoginForm();
}

function removeStoreElements() {
    let domBanner = document.getElementById("header-banner");
    let domBtnLogin = document.getElementById("btn-login");
    let domProducts = document.getElementsByClassName("products");

    domBanner.style.display = "none";
    domBtnLogin.style.display = "none";

    for (let i = 0; i < domProducts.length; i++)
        domProducts[i].style.display = "none";
}

function addLoginForm() {
    let domMain = document.getElementsByTagName("main")[0];
    domMain.innerHTML = `
    <form action="">
        <fieldset id="login-form">
            <caption>Iniciar seção</caption>
            <input type="text" placeholder="Coloque seu login">
            <input type="password" placeholder="Coloque sua senha">
            <button type="submit">Entrar</button>
        </fieldset>
    </form>`;
}

