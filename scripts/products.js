let products = {
    sw: [
        { name: "Produto XYZ", price: 60.00 },
        { name: "Produto XYZ", price: 60.00 },
        { name: "Produto XYZ", price: 60.00 },
        { name: "Produto XYZ", price: 60.00 },
        { name: "Produto XYZ", price: 60.00 },
        { name: "Produto XYZ", price: 60.00 },
    ],

    cl: [
        { name: "Controle XYZ", price: 60.00 },
        { name: "Controle e Console XYZ", price: 60.00 },
        { name: "Console XYZ", price: 60.00 },
        { name: "Controle XYZ", price: 60.00 },
        { name: "Console XYZ", price: 60.00 },
        { name: "Game Boy Color", price: 60.00 },
    ],

    ds: [
        { name: "Camisa Atari", price: 60.00 },
        { name: "Camisa SNES", price: 60.00 },
        { name: "Controle e Console XYZ", price: 60.00 },
        { name: "Controle e Console XYZ", price: 60.00 },
        { name: "Controle e Console XYZ", price: 60.00 },
        { name: "Produto XYZ", price: 60.00 },
    ],
};

function getProductCode(prefix, id) {
    let hexID = id.toString(16);
    return prefix + "00000000".substring(hexID.length) + hexID;
}

window.onload = function() {
    Object.keys(products).forEach(el => {
        let prodDiv = document.getElementById("products-" + el);

        for (let i = 0; i < products[el].length; i++) {
            let prod = document.createElement("div");

            let img = document.createElement("img");
            let title = document.createElement("p");
            let price = document.createElement("p");
            let btnSeeProd = document.createElement("button");



            /* Div styles */
            prod.className = "product";

            /* Product image */
            img.src = "images/" + getProductCode(el, i + 1) + ".png";

            /* Product title and price */
            title.textContent = products[el][i].name;
            title.className = "product-title";

            price.textContent = ("$ " + products[el][i].price).replace(".", ",");
            if (Math.floor(products[el][i].price) == products[el][i].price)
                price.textContent += ",00";

            price.className = "product-price";

            /* See product button */
            btnSeeProd.className = "btn-see-product";
            btnSeeProd.textContent = "Ver produto";


            prod.appendChild(img);
            prod.appendChild(title);
            prod.appendChild(price);
            prod.appendChild(btnSeeProd);

            prodDiv.appendChild(prod);
        }
    });
};

